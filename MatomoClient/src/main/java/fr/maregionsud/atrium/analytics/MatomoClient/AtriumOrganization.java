package fr.maregionsud.atrium.analytics.MatomoClient;

import java.util.List;

/**
 * An Atrium organization is an entity having a UAI (mainly "Établissement")
 */
public class AtriumOrganization {

	private String name;
	private String uai;


	public AtriumOrganization(String name, String uai) {
		this.name = name;
		this.uai = uai;

	}
	
	public String toString() {
		return("[Organisation: " + this.name+"]");
	}

	public String getUai() {
		return uai;
	}
	public String getName() {
		return this.name;
	}

	public static void printAtriumOrganizationList(List<AtriumOrganization> list) {
        for (AtriumOrganization organization : list) {
            System.out.println("["+organization.getUai()+", "+organization.getName()+"]");
        }
    }
}
