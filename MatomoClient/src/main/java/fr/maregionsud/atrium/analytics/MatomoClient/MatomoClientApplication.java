package fr.maregionsud.atrium.analytics.MatomoClient;

import com.mashape.unirest.http.Unirest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.List;

@SpringBootApplication
public class MatomoClientApplication implements ApplicationRunner {

    private MatomoSiteService matomoSiteService;
    private CsvParserService csvParserService;

    private final static String GENERATED_SITES_GROUP_NAME = "Recette";
    private final static String GENERATED_SITES_PLATFORM_URL = "https://recette.atrium-paca.fr/";

    @Autowired
    public MatomoClientApplication(MatomoSiteService matomoSiteService,
                                   CsvParserService csvParserService) {
        this.matomoSiteService = matomoSiteService;
        this.csvParserService = csvParserService;
    }

    public static void main(String[] args) {
        SpringApplication.run(MatomoClientApplication.class, args);
    }

    @Override
    public void run(ApplicationArguments args) throws Exception {

        initUnitRest();

        List<String> options = args.getNonOptionArgs();
        if (options.isEmpty()) {
            showHelp();
            return;
        }

        String command = options.get(0);

        switch (command) {
            case "export-generated":
                csvParserService.toCsv(
                        new OutputStreamWriter(System.out),
                        matomoSiteService.getAllGeneratedSites()
                );
                return;
            case "read-atrium-csv":
                AtriumOrganization.printAtriumOrganizationList(
                        csvParserService.parseAtriumOrganizationCsv(
                                new BufferedReader(new InputStreamReader(System.in))
                        )
                );
                return;
            case "import-atrium-csv":
                if(options.size() < 2) {
                    System.err.println("[ERREUR] L'URL est obligatoire pour la commande 'import-atrium-csv'");
                    showHelp();
                    return;
                }
                String url = options.get(1);
                String groupName = null;
                if(options.size() == 3) {
                    groupName = options.get(2);
                }

                matomoSiteService.createSiteList(
                        csvParserService.parseAtriumOrganizationCsv(
                                new BufferedReader(new InputStreamReader(System.in))
                        ),
                        groupName,
                        url
                );
                return;

            case "delete-all-generated":
                matomoSiteService.deleteSiteList(
                        matomoSiteService.getAllGeneratedSites()
                );
                return;

            case "help":
                showHelp();
                return;
                
            default:
                System.err.println("[ERREUR] '"+command+"' n'est pas une commande valide");
                showHelp();
        }

        shutdownUniRest();
    }

    private void initUnitRest() {
        Unirest.setDefaultHeader("User-Agent", "Mozilla/5.0");
    }

    private void shutdownUniRest() throws IOException {
        Unirest.shutdown();
    }

    private void showHelp() {
        System.out.println("                                                                                           ");
        System.out.println("Usage: java -jar MatomoClient-xxx <commande>                                               ");
        System.out.println("                                                                                           ");
        System.out.println("  commandes :                                                                              ");
        System.out.println("      export-generated     [> <file>]                                                      ");
        System.out.println("      read-atrium-csv      [< <file>]                                                      ");
        System.out.println("      import-atrium-csv    <url> [groupe] [< <file>]                                       ");
        System.out.println("      delete-all-generated                                                                 ");
        System.out.println("                                                                                           ");
        System.out.println("  export-generated     : Exporte en CSV, sur la sortie standard, les sites Matomo générés  ");
        System.out.println("                         par cet outil (les sites créés manuellement ne sont pas exportés) ");
        System.out.println("  read-atrium-csv      : Extrait une liste d'établissements ATRIUM décrit en CSV dans      ");
        System.out.println("                         l'entrée standard et l'affiche (à des fins de test)               ");
        System.out.println("  import-atrium-csv    : Extrait une liste d'établissements ATRIUM décrit en CSV dans      ");
        System.out.println("                         l'entrée standard et crée un site Matomo pour chacun en utilisant ");
        System.out.println("                         la valeur de <url> pour l'URL du site et la valeur de [groupe]    ");
        System.out.println("                         affecté le site à un groupe Matomo                                ");
        System.out.println("  delete-all-generated : [ATTENTION!] Supprime tous les sites Matomo créés avec cet outil  ");
        System.out.println("                                                                                           ");
        System.out.println("  exemples :                                                                               ");
        System.out.println("      java -jar MatomoClient-xxx export-generated > sites.csv                              ");
        System.out.println("      java -jar MatomoClient-xxx read-atrium-csv                                           ");
        System.out.println("      java -jar MatomoClient-xxx import-atrium-csv https://www.atrium-paca.fr/ \\          ");
        System.out.println("          établissement < etablissements.csv                                               ");
        System.out.println("      java -jar MatomoClient-xxx delete-all-generated                                      ");
        System.out.println("");
        System.out.println("");
    }
}