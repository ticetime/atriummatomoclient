package fr.maregionsud.atrium.analytics.MatomoClient;

import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.JsonNode;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;
import com.mashape.unirest.request.HttpRequest;
import org.json.JSONObject;
import org.springframework.stereotype.Service;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

@Service
public class MatomoSiteService {

    private final String TOKEN_AUTH = "b2b4cf6f5ba9a66ee131185426075b74";
    private final String MATOMO_URL = "https://matomo.atrium-paca.fr/index.php";

    /**
     * Retrieve all the Matomo sites from the Matomo REST API
     * @return
     * @throws UnirestException
     */
    public List<MatomoSite> getAllSites() throws UnirestException {
        List<MatomoSite> allMatomoSites = new ArrayList<>();

        HttpResponse<JsonNode> response =
                prepareRequest("SitesManager.getAllSites")
                .header("accept", "application/json")
                .queryString("format", "json")
                .queryString("filter_limit", -1)
                .asJson();

        if (response.getStatus() != 200) {
            throw new IllegalStateException(
                    "Something went wrong: " + response
            );
        }

        JSONObject jsonSiteList = response.getBody().getObject();
        Iterator<String> it = jsonSiteList.keys();
        while (it.hasNext()) {
            String siteId = it.next();
            JSONObject jsonSite = jsonSiteList.getJSONObject(siteId);

            allMatomoSites.add(
                    new MatomoSite(jsonSite.getString("name"), jsonSite.getInt("idsite"))
            );
        }

        return allMatomoSites;
    }

    /**
     * Retrieve all the Matomo sites that has been created by this tool from the Matomo REST API
     * @return
     * @throws UnirestException
     */
    public List<MatomoSite> getAllGeneratedSites() throws UnirestException {
        List<MatomoSite> allGeneratedSites = new ArrayList<>();

        for (MatomoSite site: getAllSites()) {
            if(site.isGenerated()) {
                allGeneratedSites.add(site);
            }
        }

        return allGeneratedSites;
    }

    /**
     * Create a Matomo sites from a list of Atrium organizations (entities with a UAI)
     * @param organizationList
     * @param groupName
     * @param platformUrl
     * @throws UnirestException
     * @throws InterruptedException
     */
    public void createSiteList(List<AtriumOrganization> organizationList,
                               String groupName,
                               String platformUrl) throws UnirestException, InterruptedException {
        for (AtriumOrganization organization : organizationList) {
            createSite(organization, groupName, platformUrl);
        }
    }

    /**
     * Create a Matomo site from an Atrium organization (entity with a UAI)
     * @param organization
     * @param groupName
     * @param platformUrl
     * @throws UnirestException
     */
    public void createSite(AtriumOrganization organization,
                           String groupName,
                           String platformUrl) throws UnirestException {
        createSite(
                organization.getName() + " |" + organization.getUai()+"|",
                groupName,
                platformUrl
        );
    }

    /**
     * Create a Matomo site with the Matomo REST API
     * @param siteName
     * @param groupName
     * @param platformUrl
     * @throws UnirestException
     */
    public void createSite(String siteName, String groupName, String platformUrl)
            throws UnirestException {
        System.out.println("[Create site "+siteName+"]");
        HttpResponse<JsonNode> response =
                prepareRequest("SitesManager.addSite")
                        .queryString("siteName", siteName)
                        .queryString("group", groupName)
                        .queryString("urls", platformUrl)
                        .queryString("format", "json")
                        .asJson();
        int responseCode = response.getStatus(); // Get the status code

        if (responseCode != 200) {
            throw new IllegalStateException("Something went wrong: " + responseCode);
        }

        System.out.println(response.getBody());
        int siteId =  response.getBody().getObject().getInt("value");
        initializeCustomDimensions(siteId);
        createCustomSegments(siteId);
    }

    /**
     * Create the 6 custom dimensions specific to ATRIUM for a Matomo site
     * @param siteId
     * @throws UnirestException
     */
    public void initializeCustomDimensions(int siteId) throws UnirestException {
        initializeCustomDimension(siteId, "Etablissement", "visit", 0);
        initializeCustomDimension(siteId, "Profil", "visit", 1);
        initializeCustomDimension(siteId, "Service", "visit", 1);
        initializeCustomDimension(siteId, "Académie", "visit", 1);
        initializeCustomDimension(siteId, "Service Tiers (OLD)", "action", 1);
        initializeCustomDimension(siteId, "Nom du Service Tiers", "visit", 1);
    }

    public void createCustomSegments(int siteId) throws UnirestException {
        createCustomSegment(siteId, "Élève", "dimension2==ELEVE");
        createCustomSegment(siteId, "Responsable élève", "dimension2==PARENT");
        createCustomSegment(siteId, "Enseignant", "dimension2==ENSEIGNANT");
        createCustomSegment(siteId, "Personnel non enseignant", "dimension2==PERSONNEL");
        createCustomSegment(siteId, "Agent", "dimension2==AGENT");
    }

    public void createCustomSegment(int siteId, String name, String definition) throws UnirestException {
        System.out.println("[Create custom segment '"+name+"']");
        HttpResponse<JsonNode> response =
                prepareRequest("SegmentEditor.add")
                        .header("accept", "application/json")
                        .queryString("format", "json")
                        .queryString("name", name)
                        .queryString("definition", definition)
                        .queryString("idSite", siteId)
                        .queryString("enabledAllUsers", 1)
                        .asJson();

        int responseCode = response.getStatus(); // Get the status code
        if (responseCode != 200) {
            throw new IllegalStateException("Something went wrong: " + responseCode);
        }

        System.out.println(response.getBody());
    }

    /**
     * Create a custom dimension for a Matomo site using the Matomo REST API
     * @param siteId
     * @param name
     * @param scope
     * @param active
     * @throws UnirestException
     */
    public void initializeCustomDimension(int siteId, String name, String scope, int active)  throws UnirestException {
        System.out.println("[Create dimension "+name+" for the site "+siteId+"]");
        HttpResponse<String> response =
                prepareRequest("CustomDimensions.configureNewCustomDimension")
                        .queryString("idSite", siteId)
                        .queryString("name", name)
                        .queryString("scope", scope)
                        .queryString("active", active)
                        .asString();

        int responseCode = response.getStatus(); // Get the status code
        if (responseCode != 200) {
            throw new IllegalStateException("Something went wrong: " + responseCode);
        }

        System.out.println(response.getBody());
    }

    /**
     * Delete a Matomo site using the Matomo REST API
     * @param siteId
     * @throws UnirestException
     */
    public void deleteSite(int siteId) throws UnirestException {
        System.out.println("[Delete site "+siteId+"]");

        HttpResponse<String> response =
                prepareRequest("SitesManager.deleteSite")
                        .queryString("idSite", siteId)
                        .asString();

        int responseCode = response.getStatus(); // Get the status code

        if (responseCode != 200) {
            throw new IllegalStateException("Something weng wrong: " + response);
        }

        System.out.println(response.getBody());
    }

    /**
     * Delete a list a Matomo sites
     * @param matomoSiteList
     * @throws UnirestException
     */
    public void deleteSiteList(List<MatomoSite> matomoSiteList) throws UnirestException {
        for (MatomoSite site : matomoSiteList) {
            deleteSite(site.getId());
        }
    }

    /**
     * Utility method that factorize the common stuff to interact with the Matomo REST API
     * @param method
     * @return
     */
    private HttpRequest prepareRequest(String method) {
        return Unirest.get(MATOMO_URL) //prepare the GET request via Unirest
                .queryString("module", "API")
                .queryString("method", method)
                .queryString("token_auth", TOKEN_AUTH);
    }
}
