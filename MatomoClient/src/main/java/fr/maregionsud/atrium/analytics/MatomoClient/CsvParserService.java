package fr.maregionsud.atrium.analytics.MatomoClient;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVPrinter;
import org.apache.commons.csv.CSVRecord;
import org.springframework.stereotype.Service;

@Service
public class CsvParserService {

    public List<AtriumOrganization> parseAtriumOrganizationCsv(BufferedReader reader) throws IOException {
        return parseAtriumOrganizationCsv(reader, ';');
    }

    /**
     * Parse a list of Atrium Organization (entity with a UAI) from CSV
     * The CSV stream is expected to have 2 columns with a header ("nom-etablissement", "uai")
     * @param reader
     * @param delimiter
     * @return
     * @throws IOException
     */
    public List<AtriumOrganization> parseAtriumOrganizationCsv(BufferedReader reader, char delimiter) throws IOException {

        List<AtriumOrganization> atriumOrganizationList = new ArrayList<>();

        CSVFormat csvFormat = CSVFormat.EXCEL
                .withDelimiter(delimiter)
                .withHeader()
                .withSkipHeaderRecord();

        CSVParser parser = new CSVParser(reader, csvFormat);

        for (CSVRecord csvRecord : parser) {
            String name = csvRecord.get("nom-etablissement");
            String uai = csvRecord.get("uai");
            AtriumOrganization atriumOrganization = new AtriumOrganization(name, uai);
            atriumOrganizationList.add(atriumOrganization);
        }

        return atriumOrganizationList;
    }

    /**
     * Export a list of Matomo sites into a Writer for the CSV format
     * The CSV header is : "UAI du site", "Id"
     * @param writer used to write the CSV data
     * @param matomoSites the list of sites to export
     * @throws IOException
     */
    void toCsv(Writer writer, List<MatomoSite> matomoSites) throws IOException {
        CSVPrinter csvPrinter = new CSVPrinter(
                writer,
                CSVFormat.EXCEL.withHeader("UAI du site", "Id")
                .withDelimiter(';')
        );

        for (MatomoSite matomoSite : matomoSites) {
            csvPrinter.printRecord(matomoSite.getUai(), matomoSite.getId());
        }

        csvPrinter.flush();
    }
}